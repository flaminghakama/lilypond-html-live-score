\version "2.19.15"

% LilyPond transcription : Mathieu "sigMate" Demange

\header {
  %title = "Tetris"
  %subtitle = "Title theme"
  %composer = "Hirokazu Tanaka"
  tagline = ##f
}

\paper {
  %#(set-paper-size "a4")
  paper-height = 8\cm
  paper-width = 100\cm
  indent = 1.9\cm
  short-indent = 1\cm
}

\new StaffGroup
{
  
  <<
    
    \new Staff \with {
      instrumentName = #"Channel 1 "
      shortInstrumentName = #"Ch. 1 "
    }
    {
      \key ees \minor
      \tempo 4 = 149
      \relative c' {
        
        d8 ees d ees bes ges' r4		| d8 ees d ees bes ges' r4		|
        r8 d r d r c r d 			| d c c b d2				|
        ees8 f ees f d d' r4			| ees,8 f ees f d d' r4			|
        r8 ees r d r bes r bes			| r ces16 d ces8 bes g2			|
        
      \key ees \major
      
        g16 a g8( g2) g4			| aes1					|
        f16 g f8( f2) f4			| f16 g g8( g2.)			| 
        d16 c bes8( bes2) g'4			| d16 ees c8( c2.)			|
        bes16 c bes8( bes2) aes4		| ces4. bes8( bes2)			|
        
        g''4. aes8( aes4) g4			| bes4. aes8( aes2)			|
        ees4. f8( f4) g4			| aes4. g8( g2)				|
        ees4. d8( d8) d8( d4)			| g4. f8( f4) c4			|
        aes'4. bes8( bes4) aes4			| f4. ees8( ees2)			\bar "|."
        
        
      }
    }
    
    \new Staff \with {
      instrumentName = #"Channel 2 "
      shortInstrumentName = #"Ch. 2 "
    }
    {
      \key ees \minor
      \relative c' {
        
        f8 ges f ges ees ees' r4		| f,8 ges f ges ees ees' r4		|
        r8 ces r bes r aes r bes		| aes16 bes aes8 aes ees f2		|
        ges8 aes ges aes f f' r4		| ges,8 aes ges aes f f' r4		|
        r8 ges r f r f r ees			| r8 ees16 f ees8 d ees2		|
        
      \key ees \major
      
        bes16 c bes8( bes2) ees4		| c1					|
        aes16 bes aes8( aes2) d4		| d16 ees bes8( bes2.)			|
        g16 bes g8( g2) bes4			| bes16 c aes8( aes2.)			|
        d,16 ees d8( d2) f4			| aes4. g8( g2)				|
        bes16 c bes8( bes2) ees4		| c1					|
        aes16 bes aes8( aes2) d4		| d16 ees bes8( bes2.)			|
        g16 bes g8( g2) bes4			| bes16 c aes8( aes2.)			|
        d,16 ees d8( d2) f4			| aes4. g8( g2)				\bar "|."
      
      }
    }
    
    \new Staff \with {
      instrumentName = #"Channel 3 "
      shortInstrumentName = #"Ch. 3 "
    }
    {
      \key ees \minor
      \clef bass
      \relative c'
      {
        
        ces8 bes ces bes ges ees, r4		| ces''8 bes ces bes ges ees, r4	|
        bes'8 f' bes, f f f f f'		| aes, ges' aes, ges' bes,2		|
        ces'8 bes ces bes bes, bes r4		| ces'8 bes ces bes bes, bes r4		|
        aes8 f' aes, f' bes, ges' bes, ges'	| bes, aes' bes, aes' ees2		|
        
      \key ees \major
      
        ees8[ ees r ees] ees[ ees r ees]	| ees8[ ees r ees] ees[ ees r ees]	|
        bes8[ bes r bes] bes[ bes r bes]	| ees8[ ees r ees] ees[ ees r ees]	|
        ees8[ ees r ees] ees[ ees r ees]	| aes,8[ aes r aes] aes[ aes r aes]	|
        bes8[ bes r bes] bes[ bes r bes]	| ees8[ ees r ees] ees[ ees r ees]	|
        ees8[ ees r ees] ees[ ees r ees]	| aes,8[ aes r aes] aes[ aes r aes]	|
        bes8[ bes r bes] bes[ bes r bes]	| ees8[ ees r ees] d[ d r d]		|
        c8[ c r c] bes[ bes r bes]		| aes8[ aes r aes] aes[ aes r aes]	|
        bes8[ bes r bes] bes[ bes r bes]	| ees8[ ees r ees] ees[ ees r ees]	\bar "|."
        
      }
    }
    
    \new DrumStaff \with {
      instrumentName = #"Channel 4 "
      shortInstrumentName = #"Ch. 4 "
    }
    {
      \drummode {
        
        r2 r4 hh8-. hh--			| r2 r4 hh8-. hh--			|
        R1					| R1					|
        r2 r4 hh8-. hh--			| r2 r4 hh8-. hh--			|
        R1					| R1					|
        hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]	| hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]|
        hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]	| hh8-. hh4-- hh-. hh-- hh8--		|
        hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]	| hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]|
        hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]	| hh8-. hh4-- hh-. hh-- hh8--		|
        hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]	| hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]|
        hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]	| hh8-. hh4-- hh-. hh-- hh8--		|
        hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]	| hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]|
        hh8-.[ hh-- r hh-.] hh-.[ hh-- r hh-.]	| hh8-. hh4-- hh-. hh-- hh8--		\bar "|."
        
      }
    }
  >>
}
