\version "2.19.36"

\include "../grob-inspector.ily"

\new RhythmicStaff {
  
 %\hide Score.MetronomeMark
 
 \time 3/4
 %\tempo 4 = 60
 c4 r c r
 %\tempo 4 = 120
 c r c r
 \tempo "Crash"
 c r c r
  
}