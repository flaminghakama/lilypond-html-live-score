Any contributions welcome! This repository is public and I'll keep working on
developping this tool as much as I can. Now that the proof-of-concept seems
good enough, there's a lot to do, like (ideas):

* nice HTML layout and design
* paper textures
* dynamic scrolling
* single-system score with horizontal scrolling
* overlaying music score on top of a video
* more SVG effects on notes
* extended music masking
* better grouping of LilyPond grobs